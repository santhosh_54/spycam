from flask import Flask,request,render_template,jsonify
from flask_socketio import SocketIO
from mongoengine import connect
from data_model import User

from keras.models import Sequential,load_model
from keras.layers import Dense,Activation,Dropout,LSTM
from keras.callbacks import ModelCheckpoint
import numpy as np
import os,collections
from load_modules1 import load_index2tag,load_tag2index
from imageio import imread
import base64,io,cv2

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

tag2index_file="./model/tag2index1.csv"
index2tag_file="./model/index2tag1.csv"
model_file="./model/12.h5"

global model,index2tag,tag2index

print("loading tag2index file")
tag2index,tag2index_len=load_tag2index(tag2index_file)

print("loading index2 tag file")
index2tag,index2tag_len=load_index2tag(index2tag_file)
print(index2tag_len)


connect(
    db='spy',
    username='admin',
    password='admin123',
    host='mongodb://admin:admin123@ds225492.mlab.com:25492/spy'
)

host='https://spycam.herokuapp.com/'
host='192.168.43.99'
port=8888
base_url=host+'/'+str(port)+'/'
import os
basedir = os.path.abspath(os.path.dirname(__file__))
app = Flask(__name__)
socketio = SocketIO(app)
print("loading weigths")
model=load_model(model_file)
model.summary()
print('lodaded')

@app.route('/')
def home():
	#socketio.emit('call',{'device_id':'','msg':'hi how are you,','loc':'t.nagar','type':index2tag[1]},broadcast=True)
	return render_template('spy_frontpage.html',base_url=base_url)

@socketio.on('device_login')
def device_login(data):
	username=data['id']
	password=data['password']
	device_id=data['mobileid']
	user=User.objects(username=username,password=password).first()
	if user:
		user.device_id=device_id
		if user.save():
			socketio.emit('device_login_success',{'device_id':device_id,'msg':'success'},broadcast=True)
		else:
			socketio.emit('device_login_success',{'device_id':device_id,'msg':'error'},broadcast=True)	
	else:
		socketio.emit('device_login_success',{'device_id':device_id,'msg':'error'},broadcast=True)


@app.route('/user_registration',methods=['POST'])
def user_registration():
	#socketio.emit('call',{'device_id':'','msg':'msg'},broadcast=True)
	data=request.form.to_dict()
	from data_model import User
	user=User(username=data['username'],address=data['address'],password=data['password'],phone_no=data['phone_no'])
	if user.save():
		return jsonify(response='success')
	else:
		return jsonify(response='error')

@app.route('/user_login',methods=['POST'])
def user_login():
	data=request.form.to_dict()
	username=data['username']
	password=data['password']
	user=User.objects(username=username,password=password).first()
	if user:
		return render_template('spy_webcam.html',base_url=base_url,username=username,password=password,address=user.address)
	else:
		return render_template('spy_frontpage.html',base_url=base_url)

@app.route('/send_frames',methods=['POST'])
def send_frames():
	data=request.json['data']
	event=request.json['event']
	X=np.zeros((32,60,80,1),dtype=np.uint8)
	for i in range(len(data)):
		string_data=data[i].split(',')[1]
		#print(string_data)
		img=imread(io.BytesIO(base64.b64decode(string_data)))
		cv_img=cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
		cv_img=cv2.resize(cv_img,(80,60)).reshape(60,80,1)
		X[i]=cv_img
	X=X.reshape((1,32,60,80,1))
	"""
	print("loading weigths")
	model=load_model(model_file)
	model.summary()
	print('lodaded')
	#print(model.predict(X))
	#print(X.shape)
	"""
	pred=collections.Counter()
	for i in range(3):
		y=model.predict(X)
		print(y,y.shape)
		y[0][2]-=0.40
		pred[index2tag[np.argmax(y)]]+=1
	new_event=pred.most_common(1)
	print(new_event)
	if event!=new_event and new_event!='Normal':
		socketio.emit('call',{'device_id':'','msg':'we have a problem in taramani','loc':'t.nagar','type':pred.most_common(1)},broadcast=True)
	return jsonify(response='success',event=pred.most_common(1))
if __name__ == "__main__":
	socketio.run(app,host=host,debug=True)

