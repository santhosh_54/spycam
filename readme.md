#SpyCam

##website link:
https://spycam.herokuapp.com/

##Mobile App:
you can find mobile app in, https://drive.google.com/open?id=1-u0638zfLtJub_NRyUClM7XybgOovfY7

##Demo video:
will be provided soon

##objective
    Our objective is to build an application capable of detecting abnormal change in human health condition,
    road accidents and other activities that are related to violence against women’s from CCTV footage,
    Using Convolution Neural Network and OpenCV

##Platforms:
    Front-end             : Android, Web
    Back-end              : Flask
    Processing modules    : OpenCV(To process CCTV video) and CNN Model with LSTM build  using Keras.
    Dataset               : Various Images taken from Google-Images will be collected and then used as dataset to  
                            train the model.
##Pros:
    Our application automatically abnormal activities, without any human help and alerts concern authority.

##Cons:
    Accuracy of  detection of abnormal activities depends on Training time and on dataset used to train the model.

##Developers

    Nikil            Front-end developer
    Natarajan M      Front-end developer
    Keerthivaasan    Android Developer
    S Santhosh Kumar Back-end,deep learning