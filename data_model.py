from mongoengine import *
class User(Document):
    sno=SequenceField()
    username=StringField(required=True)
    address=StringField(required=True)
    password=StringField(required=True)
    device_id=StringField()
    phone_no=StringField(required=True)
    
    def __repr__(self):
        return '<User %r>' %self.username



