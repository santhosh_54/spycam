import csv,os
import numpy as np
def load_tag2index(file_path):
    tag2index=dict()
    with open(file_path,'r') as data_file:
        reader=csv.reader(data_file)
        for row in reader:
            if len(row)!=2:
                continue
            tag,index=row
            index=int(index)
            tag2index[tag]=index
    return tag2index,len(tag2index)


def load_index2tag(file_path):
    index2tag=dict()
    with open(file_path,'r') as data_file:
        reader=csv.reader(data_file)
        for row in reader:
            if len(row)!=2:
                continue
            index,tag=row
            index=int(index)
            index2tag[index]=tag
    return index2tag,len(index2tag)

