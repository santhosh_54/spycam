from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager
from app import app,db
class User(db.Model):
    __tablename__='user'
    sno=db.Column(db.Integer,primary_key=True, autoincrement=True)
    username=db.Column(db.String,unique=True)
    address=db.Column(db.String,nullable=False)
    password=db.Column(db.String,nullable=False)
    device_id=db.Column(db.String,nullable=True,default='')
    phone_no=db.Column(db.String,nullable=False)
    
    def __repr__(self):
        return '<User %r>' %self.username

manager=Manager(app)
migrate = Migrate(app, db)
manager.add_command('db', MigrateCommand)